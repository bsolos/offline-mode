# Offline Mode
A simple Minecraft mod that disables online mode on the internal server. For example, this can be used to play on a local network without an internet connection.

## Downloaad
https://www.curseforge.com/minecraft/mc-mods/offline-mode

## Build
JDK 1.8 is required for building this mod. Verify that your JAVA_HOME environment variable is set correctly, and the JDK 1.8 bin is on PATH. Go to the directory of the version for which you want to build, then run:

For Linux (tested) and other UNIX-like (untested): `./gradlew build --refresh-dependencies`

For Windows (untested): `.\gradlew.bat build --refresh-dependencies` 

The mod jar will be located in `./build/libs`
