/*
 * Copyright 2022 bsolos
 *
 * OfflineModeMod.java
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */

package bsolos.offlinemode;

import net.minecraft.server.MinecraftServer;
import net.minecraft.server.integrated.IntegratedServer;

import org.apache.logging.log4j.Logger;

import bsolos.offlinemode.proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerAboutToStartEvent;

@Mod(modid = OfflineModeMod.MODID, version = OfflineModeMod.VERSION, acceptableRemoteVersions="*")
public class OfflineModeMod
{
    public static final String MODID = "offlinemode";
    public static final String VERSION = "1.1";
 
    public static Logger logger;
    
    @SidedProxy(clientSide = "bsolos.offlinemode.proxy.ClientProxy", serverSide = "bsolos.offlinemode.proxy.ServerProxy")
    public static CommonProxy proxy;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent e) {
    	logger = e.getModLog();
    }
    
    @EventHandler
    public void serverAboutToStart(FMLServerAboutToStartEvent e) {
    	proxy.serverAboutToStart(e);
    }
}
