# Setup development environment
Follow the build instructions, but instead of actually building the mod, run:

For Linux (tested) and other UNIX-like (untested): `./gradlew setupDecompWorkspace <IDE> --refresh-dependencies`

For Windows (untested): `.\gradlew.bat setupDecompWorkspace <IDE> --refresh-dependencies`

Where `<IDE>` is either `eclipse` or `idea` 
